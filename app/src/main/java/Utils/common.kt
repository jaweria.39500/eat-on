package Utils


import ModelClasses.User
import android.content.SharedPreferences
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics

import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.BuildConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import pk.edu.iqra.eaton.Database.CartItem
import pk.edu.iqra.eaton.ModelClasses.Category.AddOn
import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.ModelClasses.Category.Menu
import pk.edu.iqra.eaton.ModelClasses.Category.Size
import java.lang.StringBuilder
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.*

object common {


    var currentUser: User?=null
    var user_Id:String?=null
    var category_selected: Menu?=null
    var food_selected: Food?=null
    var changedCart: CartItem?=null
    var serviceNotavailableBCO:String?="some issue"
    var isServiceAvailable:Boolean?=true
    var sharedPreferences: SharedPreferences?=null

    var curEvent:String?=null
    var firebaseAnalytics: FirebaseAnalytics =  Firebase.analytics
    @RequiresApi(Build.VERSION_CODES.N)
    val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
    @RequiresApi(Build.VERSION_CODES.N)
    val currentDate = sdf.format(Date())

    val remoteConfig = Firebase.remoteConfig

    val configSettings = FirebaseRemoteConfigSettings.Builder()
        .setDeveloperModeEnabled(BuildConfig.DEBUG)
        .build()


    fun formatPrice(price: Double): String
    {
        if(price != 0.0)
        {
            var df=DecimalFormat("#,###0.0")
            df.roundingMode=RoundingMode.HALF_UP
            var finalPrice=StringBuilder(df.format(price)).toString()
            return finalPrice
        }
        else
            return "00.0"
    }
    fun calculateExtraPrice(userSelectedSize: Size?, userSelectedAddOn: MutableList<AddOn>?):Double
    {
        var result: Double = 0.0
        if(userSelectedSize ==null && userSelectedAddOn==null)
        {
            return 0.0
        }
        else if (userSelectedSize ==null)
        {
            for(addOn in userSelectedAddOn!!)
                result +=addOn.price.toDouble()
            return result

        }
        else if(userSelectedAddOn==null)
        {
            result=userSelectedSize.price.toDouble()
            return result
        }
        else
        {
            result=userSelectedSize.price.toDouble()
            for(addOn in userSelectedAddOn!!)
                result +=addOn.price.toDouble()
            return result
        }
    }

    fun logEvent(event:String)
    {
        val bundle = Bundle()
        bundle.putString(Constants.user_Id, common.user_Id)
        bundle.putString(Constants.user, common.currentUser!!.name)
        bundle.putString(Constants.datetime, common.currentDate)
        firebaseAnalytics.logEvent(event, bundle)

       
    }



}