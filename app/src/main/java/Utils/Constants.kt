package Utils

object Constants
{

    const val Pref_Name="pref_food_app"
    const val user_Name="User_Name"
    const val user="User_Name"
    const val user_Id="User_Id"
    const val event="Event"
    const val datetime="DateTime"

    const val Key_btn_Place_Order="btn_Place_Order"
    const val Key_Service_Unavailability_Reason="ServiceUnavailabilityReason"
    const val Key_is_Service_Available="isServiceAvailable"
}