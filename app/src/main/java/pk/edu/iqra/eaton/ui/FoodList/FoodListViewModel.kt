package pk.edu.iqra.eaton.ui.FoodList

import CallBack.ICallBackListener
import ModelClasses.Category
import ModelClasses.User
import Utils.common
import Utils.showToast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import pk.edu.iqra.eaton.CallBack.IFoodCallBackListener

import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.ModelClasses.Category.Menu

class FoodListViewModel : ViewModel(){

    var mutableLiveFoodList:  MutableLiveData<List<Food>>?=null

    fun getmutableLiveFoodList(): MutableLiveData<List<Food>>
    {
            if(mutableLiveFoodList ==null)
            {
                mutableLiveFoodList= MutableLiveData()
            }
            mutableLiveFoodList!!.value=common.category_selected!!.foods
            return mutableLiveFoodList!!

    }


}