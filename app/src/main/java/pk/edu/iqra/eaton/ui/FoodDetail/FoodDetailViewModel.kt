package pk.edu.iqra.eaton.ui.FoodDetail

import Utils.common
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.ModelClasses.Category.Menu

class FoodDetailViewModel : ViewModel() {

    var mutableLiveDataFood: MutableLiveData<Food>?=null
    fun getmutableLiveFoodList(): MutableLiveData<Food>
    {
        if(mutableLiveDataFood ==null)
        {
            mutableLiveDataFood= MutableLiveData()
        }
        mutableLiveDataFood!!.value= common.food_selected
        return mutableLiveDataFood!!

    }
}