package pk.edu.iqra.eaton

import ModelClasses.User
import Utils.Constants
import Utils.common
import Utils.showToast
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*
import java.util.*

class LogInActivity : AppCompatActivity() {
    lateinit var context: Context


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)

        context = this
        common.sharedPreferences = getSharedPreferences(Constants.Pref_Name, Context.MODE_PRIVATE)


        var database = Firebase.database
        var table_user = database.getReference("user")

        common.curEvent="LOG_IN"
        btnLogIn2.setOnClickListener {
            var phNum = txtPhNumber.getText().toString()
            var password = txtPassword.getText().toString()

            if(common.curEvent=="LOG_IN") {


                val postListener = object : ValueEventListener {

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (phNum != null || password != null) {
                            if (dataSnapshot.child(phNum).exists()) {
                                var user = dataSnapshot.child(phNum).getValue(User::class.java)

                                if (user != null) {
                                    if (user.password == password) {

                                        common.currentUser = user
                                        common.user_Id = phNum
                                        saveUserPreferences()
                                        showToast(context, "LogIn Successfull")
                                        var home_intent = Intent(context, HomeActivity::class.java)
                                        startActivity(home_intent)
                                        finish()
                                        ///////////////Add Event to Analytics Log
                                        common.logEvent("LogIn")

                                    } else {
                                        showToast(context, "Incorrect Password!")
                                    }
                                }


                            } else {
                                showToast(context, "User dosn't exist!")
                            }
                        } else {
                            showToast(context, "Please enter Phone Number and Password")
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                        showToast(context, "db error")
                    }

                }
                table_user.addValueEventListener(postListener)
            }

        }
    }

    fun saveUserPreferences() {
        val editor = common.sharedPreferences!!.edit()
        editor.putString(Constants.user_Id, common.user_Id)
        editor.commit()
    }

    fun checkIfUserAlreadyRegistered() {
        val userId = common.sharedPreferences!!.getString(Constants.user_Id, "Invalid UserId")
        if (!userId!!.contentEquals("Invalid UserId")) {
            var intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()

        }
    }
}