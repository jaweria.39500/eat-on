package pk.edu.iqra.eaton.ModelClasses.Category

data class Size (
    var name:String="",
    var price:Double=0.0
)