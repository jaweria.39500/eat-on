package pk.edu.iqra.eaton.ui.Home

import CallBack.ICallBackListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import pk.edu.iqra.eaton.ModelClasses.Category.Menu

class HomeViewModel : ViewModel(),ICallBackListener {

     var ListMutableLiveDataCategory: MutableLiveData<List<Menu>>?=null
    lateinit var errorMessage: MutableLiveData<String>
    lateinit var loadCallBackListener:ICallBackListener

    override fun onLoadFailure(message: String)
    {
        errorMessage.value=message
    }

    override fun onLoadSuccess(menu: List<Menu>)
    {
        ListMutableLiveDataCategory?.value=menu
    }

    val categoryList:LiveData<List<Menu>>
    get()
    {
        if(ListMutableLiveDataCategory == null)
        {
            ListMutableLiveDataCategory=MutableLiveData()
            errorMessage= MutableLiveData()
            loadList()
        }
        return ListMutableLiveDataCategory!!
    }
    init
    {
         loadCallBackListener=this
    }



    private fun loadList()
    {
        var tempList=ArrayList<Menu>()
        var database=   Firebase.database
        var table_category=database.getReference("Category")

        val postListener = object : ValueEventListener {

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                    for (item in dataSnapshot.children) {
                        if(item.getValue<Menu>(Menu::class.java) !=null)
                            {
                                var model = item.getValue<Menu>(Menu::class.java)
                                tempList.add(model!!)
                            }
                    }
                    loadCallBackListener.onLoadSuccess(tempList)

            }
            override fun onCancelled(databaseError: DatabaseError)
            {
                loadCallBackListener.onLoadFailure(databaseError.message)
            }

        }
        table_category.addListenerForSingleValueEvent(postListener)
    }
}