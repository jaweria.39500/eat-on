package pk.edu.iqra.eaton.Adapter

import Utils.common
import Utils.showToast
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import org.greenrobot.eventbus.EventBus
import pk.edu.iqra.eaton.CallBack.IRecyclerItemClickListener
import pk.edu.iqra.eaton.Database.CartItem
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.EventBus.CountCartEvent
import pk.edu.iqra.eaton.EventBus.FoodItemClick
import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.R
import rx.Scheduler
import java.lang.StringBuilder

class FoodListAdapter(var context: Context, var foodList:List<Food> ): RecyclerView.Adapter<FoodListAdapter.FoodViewHolder>(){

    private var compositeDisposable:CompositeDisposable?=null
    private var cartDataSource:ICartDataSource?=null

    init
    {
        compositeDisposable= CompositeDisposable()
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(context).cartDao())
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int)
    {
        Glide.with(context).load(Uri.parse(foodList.get(position).image!!))
            .into(holder.image_food!!)
        holder.txtFoodName!!.setText(foodList.get(position).name)
        val price=StringBuilder("Rs. ").append(foodList.get(position).price)
        holder.txtFoodPrice!!.setText(price)

        holder.setListener(object : IRecyclerItemClickListener{
            override fun onItemClick(view: View, position: Int)
            {
                common.food_selected=foodList.get(position)
                EventBus.getDefault().postSticky(FoodItemClick(true,foodList.get(position)))
            }
        }
        )
        holder.image_cart!!.setOnClickListener{
            var cart=CartItem()
            cart.userId=common.user_Id
            cart.userPhoneNumber=common.user_Id
            cart.foodId=foodList.get(position).id
            cart.foodName=foodList.get(position).name
            cart.foodImage=foodList.get(position).image
            cart.foodId=foodList.get(position).id
            cart.foodPrice=foodList.get(position).price.toDouble()
            cart.foodExtraPrice=0.0
            cart.foodQuantity=1
            cart.addOn="DEFAULT"
            cart.size="DEFAULT"


           cartDataSource!!.getItemWithOptionsInCart(common.user_Id!!,cart.foodId!!,cart.size!!,cart.addOn!!).subscribeOn(Schedulers.io())
               .observeOn(AndroidSchedulers.mainThread())
               .subscribe(object:SingleObserver<CartItem>{
                   override fun onSuccess(carItemFromDb: CartItem)
                   {

                       if(carItemFromDb.equals(cart))
                       {
                           carItemFromDb.foodExtraPrice=cart.foodExtraPrice
                           carItemFromDb.addOn=cart.addOn
                           carItemFromDb.size=cart.size
                           carItemFromDb.foodQuantity=carItemFromDb.foodQuantity+cart.foodQuantity

                           cartDataSource!!.update(carItemFromDb).subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribe(object:SingleObserver<Int>{
                                   override fun onSuccess(t: Int)
                                   {
                                       showToast(context!!,"Update Cart Successful!")
                                       EventBus.getDefault().postSticky(CountCartEvent(true))

                                   }

                                   override fun onSubscribe(d: Disposable)
                                   {

                                   }

                                   override fun onError(e: Throwable)
                                   {
                                       showToast(context!!,"Cart Error"+e.message)
                                   }


                               })

                       }
                       else
                       {
                           compositeDisposable!!.add(cartDataSource!!.insertOrreplaceAll(cart).subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribe({
                                   showToast(context,"Inserted into Cart!")
                                   })
                           )
                       }
                   }

                   override fun onSubscribe(d: Disposable)
                   {

                   }

                   override fun onError(e: Throwable)
                   {
                       if(e.message!!.contains("empty"))
                       {
                           compositeDisposable!!.add(cartDataSource!!.insertOrreplaceAll(cart)
                               .subscribeOn(Schedulers.io())
                               .observeOn(AndroidSchedulers.mainThread())
                               .subscribe({
                                   showToast(context,"Inserted into Cart!")
                                   EventBus.getDefault().postSticky(CountCartEvent(true))
                               },{t: Throwable? ->

                                   showToast(context,"Insert cart "+t!!.message)
                               }

                               )

                           )

                       }
                       else
                       {
                           showToast(context,"Cart Error!")
                       }
                   }

               })

        }
    }
    fun onStop()
    {
        if(compositeDisposable !=null)
        {
            compositeDisposable!!.clear()
        }
    }
    override fun getItemCount(): Int
    {
        return foodList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder
    {
        return FoodViewHolder(LayoutInflater.from(context).inflate(R.layout.food_item,parent,false))

    }


    inner class FoodViewHolder(view: View): RecyclerView.ViewHolder(view),
        View.OnClickListener
    {
        var txtFoodName: TextView?=null
        var txtFoodPrice: TextView?=null
        var image_food: ImageView?=null
        var image_cart: ImageView?=null
        internal var listener: IRecyclerItemClickListener?=null


        init
        {
            txtFoodName=view.findViewById(R.id.txtFoodName) as TextView
            txtFoodPrice=view.findViewById(R.id.txtFoodPrice) as TextView
            image_food=view.findViewById(R.id.image_food) as ImageView
            image_cart=view.findViewById(R.id.image_cart) as ImageView
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?)
        {
            listener!!.onItemClick(v!!,adapterPosition)
        }

        fun setListener(listener: IRecyclerItemClickListener)
        {
            this.listener=listener
        }

    }
}