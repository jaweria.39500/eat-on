package pk.edu.iqra.eaton.ModelClasses.Category

data class AddOn (
    var name:String="",
    var price:Double=0.0
)