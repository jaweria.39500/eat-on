package pk.edu.iqra.eaton.Database

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class localCartDataSource(private val cartDao:ICartDao):ICartDataSource {

    override fun getAllCart(userId: String): Flowable<List<CartItem>>
    {
        return cartDao.getAllCart(userId)
    }

    override fun countItemCart(userId: String): Single<Int>
    {
        return cartDao.countItemCart(userId)
    }

    override fun sumPricce(userId: String): Single<Double>
    {
        return cartDao.sumPricce(userId)
    }

    override fun getItemInCart(userId: String, foodId: String): Single<CartItem>
    {
        return cartDao.getItemInCart(userId,foodId)
    }



    override fun insertOrreplaceAll(vararg cart: CartItem): Completable
    {
        return cartDao.insertOrreplaceAll(*cart)
    }

    override fun update( cart: CartItem): Single<Int>
    {
        return cartDao.update(cart)
    }

    override fun deleteCart(cart: CartItem): Single<Int>
    {
        return cartDao.deleteCart(cart)
    }

    override fun cleanCart(userId: String): Single<Int>
    {
        return cartDao.cleanCart(userId)
    }

    override fun getItemWithOptionsInCart(
        userId: String,
        foodId: String,
        foodSize: String,
        foodAddOn: String
    ): Single<CartItem> {
        return cartDao.getItemWithOptionsInCart(userId,foodId,foodSize,foodAddOn)
    }
}