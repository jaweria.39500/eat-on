package pk.edu.iqra.eaton.ui.Cart

import Utils.common
import Utils.showToast
import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import pk.edu.iqra.eaton.Database.CartItem
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.ModelClasses.Category.Food

class CartViewModel : ViewModel() {

    private var compositeDisposable: CompositeDisposable?=null
    private var cartDataSource: ICartDataSource?=null

    var mutableLiveDataFood: MutableLiveData<List<CartItem>>?=null

    init
    {
        compositeDisposable = CompositeDisposable()
    }

    fun getmutableLiveFoodList(): MutableLiveData<List<CartItem>>
    {
        if(mutableLiveDataFood ==null)
        {
            mutableLiveDataFood= MutableLiveData()
        }
        getCartList()
        return mutableLiveDataFood!!

    }

    fun initCartDataSource(context: Context)
    {
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(context).cartDao())
    }
    fun getCartList()
    {

        compositeDisposable!!.add(cartDataSource!!.getAllCart(common.user_Id!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({cartItems->
                mutableLiveDataFood!!.value=cartItems
            },{t: Throwable? ->
                mutableLiveDataFood!!.value=null
            })
        )
    }
    fun onStop()
    {

        compositeDisposable!!.clear()

    }
}