package pk.edu.iqra.eaton.ModelClasses.Category

import androidx.lifecycle.MutableLiveData
import pk.edu.iqra.eaton.ModelClasses.Category.AddOn

data class Food (
    var addon: List<AddOn>?=null,
    var description:String="",
    var id:String="",
    var image:String="",
    var name:String="",
    var price:Double=0.0,
    var size:List<Size>?=null,
    var user_selected_addOn: MutableList<AddOn>?=null,
    var user_selected_size: Size?=null
)