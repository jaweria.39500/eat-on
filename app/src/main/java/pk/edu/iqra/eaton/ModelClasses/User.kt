package ModelClasses

data class User(
    var address:String="",
    var name:String="",
    var password: String=""
)