package pk.edu.iqra.eaton.ui.Cart

import Utils.Constants
import Utils.common
import Utils.showToast
import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController

import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.andremion.counterfab.CounterFab
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.fragment_cart.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pk.edu.iqra.eaton.Adapter.CartAdapter
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.EventBus.HideFabCart
import pk.edu.iqra.eaton.EventBus.UpdateItemCart
import pk.edu.iqra.eaton.HomeActivity
import pk.edu.iqra.eaton.R
import pk.edu.iqra.eaton.R.*

class CartFragment : Fragment() {

    var home:HomeActivity= HomeActivity()
    private var compositeDisposable: CompositeDisposable= CompositeDisposable()
    private var cartDataSource: ICartDataSource?=null
    var recyclerviewState:Parcelable?=null
    var recyclerViewCart:RecyclerView?=null
    var btnPlaceOrder: Button?=null
    var btnDone:MaterialButton?=null
    var navController:NavController?=null
    var txtEmptyCart: TextView? = null
    var txtTotalPrice: TextView? = null
    var group_holder:CardView?=null
    var address_group_holder:CardView?=null
    var editAdress: EditText? = null

    

    private lateinit var cartViewModel: CartViewModel
    

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        EventBus.getDefault().postSticky(HideFabCart(true))

        cartViewModel =
            ViewModelProviders.of(this).get(CartViewModel::class.java)

        cartViewModel.initCartDataSource(requireContext())
        val root = inflater.inflate(layout.fragment_cart, container, false)

        initView(root)

        cartViewModel.getmutableLiveFoodList().observe(viewLifecycleOwner, Observer {
            if(it==null || it.isEmpty())
            {
                recyclerViewCart!!.visibility=View.GONE
                group_place_holder.visibility=View.GONE
                txtEmptyCart!!.visibility=View.VISIBLE
            }
            else
            {
                recyclerViewCart!!.visibility=View.VISIBLE
                group_place_holder.visibility=View.VISIBLE
                txtEmptyCart!!.visibility=View.GONE
                val adapter= context?.let { it1 -> CartAdapter(it1,it) }

                recyclerViewCart!!.adapter=adapter

            }
        })


        btnPlaceOrder!!.setOnClickListener{
            if(common.isServiceAvailable!! ==false)
            {
               showToast(requireContext(),"Service is not available beacause of"+common.serviceNotavailableBCO)
            }
            else
            {
                address_group_holder!!.visibility=View.VISIBLE
                editAdress!!.hint=common.currentUser!!.address
                btnPlaceOrder!!.visibility=View.GONE
            }


        }

        var database=   Firebase.database
        var table_user=database.getReference("user")

        common.curEvent="PLACE_ORDER"

        btnDone!!.setOnClickListener {
            var user = common.currentUser
            var address = editAdress!!.getText().toString()
            user!!.address = address

            if(common.curEvent=="PLACE_ORDER") {
                table_user.child(common.user_Id!!).setValue(user)
                    .addOnSuccessListener {
                        address_group_holder?.visibility = View.GONE
                        showToast(requireContext(), "Your order will be delivered soon!")
                        navController!!.navigate(R.id.nav_home)
                        cartDataSource!!.cleanCart(common.user_Id!!)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : SingleObserver<Int> {
                                override fun onSuccess(t: Int) {
                                    address_group_holder?.visibility = View.GONE
                                    ///////////////Add Event to Analytics Log
                                    common.logEvent("PlaceOrder")
                                }

                                override fun onSubscribe(d: Disposable) {

                                }

                                override fun onError(e: Throwable) {

                                }
                            })
                    }
                    .addOnFailureListener {
                        showToast(requireContext(), "Error")
                    }
            }
        }
        return root
    }
    private fun initView(root:View)
    {



        navController =  findNavController(this)
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(requireContext()).cartDao())

        recyclerViewCart=root.findViewById(R.id.recycler_view_place_holder) as RecyclerView
        recyclerViewCart?.setHasFixedSize(true)
        var layoutManager=LinearLayoutManager(context)

        recyclerViewCart?.layoutManager= layoutManager
        recyclerViewCart?.addItemDecoration(DividerItemDecoration(context,layoutManager.orientation))

        txtEmptyCart=root.findViewById(R.id.txtEmptyCart) as TextView
        txtTotalPrice=root.findViewById(R.id.txtTotalPrice) as TextView
        group_holder=root.findViewById(R.id.group_place_holder) as CardView


        btnPlaceOrder=root.findViewById(R.id.action_place_order) as Button
        btnDone=root.findViewById(R.id.action_done) as MaterialButton
        address_group_holder=root.findViewById(R.id.group_address) as CardView
        editAdress=root.findViewById(R.id.editAddress) as EditText

        home.getRemoteConfigValues(btnPlaceOrder!!)

        
    }

    override fun onStart()
    {
        super.onStart()
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this)
    }

    override fun onStop()
    {
        super.onStop()
        cartViewModel.onStop()
        compositeDisposable.clear()
        EventBus.getDefault().postSticky(HideFabCart(false))
        if((EventBus.getDefault().isRegistered(this))) {
            EventBus.getDefault().unregister(this)

        }
    }

    override fun onResume()
    {
        calculateTotalPrice()
        super.onResume()
    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun onUpdateItemCart(event:UpdateItemCart)
    {
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(requireContext()).cartDao())

        showToast(requireContext(),event.cartItem.foodName!!)
        if(event.cartItem!=null)
        {
            recyclerviewState=recyclerViewCart!!.layoutManager!!.onSaveInstanceState()
            cartDataSource!!.update(event.cartItem).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: SingleObserver<Int> {
                    override fun onSuccess(t: Int)
                    {
                        calculateTotalPrice()
                        recyclerViewCart!!.layoutManager!!.onRestoreInstanceState(recyclerviewState)
                    }

                    override fun onSubscribe(d: Disposable)
                    {
                        TODO("Not yet implemented")
                    }

                    override fun onError(e: Throwable)
                    {
                        showToast(context!!,"Update Cart"+e.message)
                    }

                })
        }
    }

    private fun calculateTotalPrice()
    {
        cartDataSource!!.sumPricce(common.user_Id!!)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object: SingleObserver<Double> {
                override fun onSuccess(price: Double)
                {
                    txtTotalPrice!!.text=StringBuilder("Total : Rs. ").append(common.formatPrice(price))
                }

                override fun onSubscribe(d: Disposable)
                {

                }

                override fun onError(e: Throwable)
                {
                    showToast(context!!,"Sum Cart"+e.message)
                }

            })

    }





}
