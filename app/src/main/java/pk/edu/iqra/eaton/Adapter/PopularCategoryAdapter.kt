package pk.edu.iqra.eaton.Adapter

import pk.edu.iqra.eaton.EventBus.CategoryClick
import Utils.common
import Utils.showToast
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView


import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import org.greenrobot.eventbus.EventBus
import pk.edu.iqra.eaton.CallBack.IRecyclerItemClickListener
import pk.edu.iqra.eaton.ModelClasses.Category.Menu
import pk.edu.iqra.eaton.R


class PopularCategoryAdapter(var context: Context, var categoryList:List<Menu> ): RecyclerView.Adapter<PopularCategoryAdapter.CategoryViewHolder>(){
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int)
    {

         Glide.with(context).load(Uri.parse(categoryList.get(position).image!!)).into(holder.categoryImage!!)
         holder.categoryName!!.setText(categoryList.get(position).name)

         holder.setListener(object : IRecyclerItemClickListener{
             override fun onItemClick(view: View, position: Int)
             {
                 showToast(context,categoryList.get(position).name)
                 common.category_selected=categoryList.get(position)
                 EventBus.getDefault().postSticky(CategoryClick(true,categoryList.get(position)))
             }
         }
         )

    }

    override fun getItemCount(): Int
    {
        return categoryList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder
    {
        return CategoryViewHolder(LayoutInflater.from(context).inflate(R.layout.category_item,parent,false))

    }


    inner class CategoryViewHolder(view: View): RecyclerView.ViewHolder(view),
        View.OnClickListener {
        var categoryName: TextView?=null
        var categoryImage:CircleImageView?=null
        internal var listener:IRecyclerItemClickListener?=null

        fun setListener(listener:IRecyclerItemClickListener)
        {
            this.listener=listener
        }
        init {
            categoryName=view.findViewById(R.id.txtCategoryName) as TextView
            categoryImage=view.findViewById(R.id.category_image) as CircleImageView
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?)
        {
            listener!!.onItemClick(v!!,adapterPosition)
        }


    }
}