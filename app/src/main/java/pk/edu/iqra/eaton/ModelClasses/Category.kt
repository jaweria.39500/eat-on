package ModelClasses

data class Category (
    var image:String="",
    var name:String="",
    var food_id:String="",
    var menu_id:String=""
)