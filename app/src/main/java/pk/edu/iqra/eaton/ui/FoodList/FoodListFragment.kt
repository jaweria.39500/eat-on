package pk.edu.iqra.eaton.ui.FoodList

import Utils.common
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.Unbinder
import pk.edu.iqra.eaton.Adapter.FoodListAdapter
import pk.edu.iqra.eaton.R

class FoodListFragment : Fragment() {

    private lateinit var foodListViewModel: FoodListViewModel
    var recyclerViewFoodList: RecyclerView? = null
    var unbinder: Unbinder?=null
    var adapter:FoodListAdapter?=null

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        foodListViewModel =
            ViewModelProviders.of(this).get(FoodListViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_food_list, container, false)
        unbinder= ButterKnife.bind(this,root)
        initView(root)

        foodListViewModel.getmutableLiveFoodList().observe(viewLifecycleOwner, Observer {

            adapter = context?.let { it1 -> FoodListAdapter(it1, it) }

            recyclerViewFoodList!!.adapter = adapter
        })
        return root
    }

    override fun onStop() {
        if(adapter!=null)
            adapter!!.onStop()
        super.onStop()
    }
    private fun initView(root: View?)
    {
        recyclerViewFoodList = root!!.findViewById(R.id.recycler_view_food_list) as RecyclerView
        recyclerViewFoodList?.setHasFixedSize(true)
        recyclerViewFoodList?.layoutManager = LinearLayoutManager(context)

        (activity as AppCompatActivity).supportActionBar!!.title= common.category_selected!!.name


    }
}
