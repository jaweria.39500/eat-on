package pk.edu.iqra.eaton.CallBack

import pk.edu.iqra.eaton.ModelClasses.Category.Food

interface IFoodCallBackListener {
    fun onLoadSuccess(category:List<Food>)
    fun onLoadFailure(message:String)
}
