package pk.edu.iqra.eaton.ui.SignOut

import Utils.Constants
import Utils.common
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.google.android.material.button.MaterialButton
import pk.edu.iqra.eaton.MainActivity
import pk.edu.iqra.eaton.R

class signOutshowFragment : Fragment() {

    private lateinit var sigOutViewModel: SignOutViewModel

    var btnyes:MaterialButton?=null
    var btnno:MaterialButton?=null
    lateinit var  navController: NavController

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sigOutViewModel =
            ViewModelProviders.of(this).get(SignOutViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_sign_out, container, false)

        navController = findNavController(this)
      initView(root)
        btnyes!!.setOnClickListener{
            signOut()
        }
        btnno!!.setOnClickListener{
            navController.navigate(R.id.nav_home)
        }
        return root
    }
    fun initView(root: View)
    {
        btnyes=root.findViewById(R.id.action_yes) as MaterialButton
        btnno=root.findViewById(R.id.action_no) as MaterialButton


    } fun signOut()
    {
        var editor= common.sharedPreferences!!.edit()
        editor.clear().commit()

        var intent= Intent(context, MainActivity::class.java)
        startActivity(intent)
        onStop()
    }

    override fun onStop() {
        super.onStop()
    }
}