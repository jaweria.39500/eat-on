package pk.edu.iqra.eaton.ui.FoodDetail

import Utils.common
import Utils.showToast
import android.annotation.SuppressLint
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.andremion.counterfab.CounterFab
import com.bumptech.glide.Glide
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_food_detail.*
import org.greenrobot.eventbus.EventBus
import pk.edu.iqra.eaton.Database.CartItem
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.EventBus.CountCartEvent
import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.R
import java.lang.StringBuilder

class FoodDetailFragment : Fragment(), TextWatcher {

    lateinit var addOn_bottom_Sheet_Dialog:BottomSheetDialog
    var img_food: ImageView?=null
    var btnCart:CounterFab?=null
    var btnrating:FloatingActionButton?=null
    var txtfoodName:TextView?=null
    var txtfoodDescription:TextView?=null
    var txtfoodPrice:TextView?=null
    var elegantNumber:ElegantNumberButton?=null
    var ratingbar:RatingBar?=null
    var radio_group_food_size:RadioGroup?=null
    var image_addOn:ImageView?=null
    var chip_Group_user_selected_addOn:ChipGroup?=null

    var chip_group_addOn:ChipGroup?=null
    var editSearch:EditText?=null



    private var compositeDisposable: CompositeDisposable?=null
    private var cartDataSource: ICartDataSource?=null

    private lateinit var foodDetailViewModel: FoodDetailViewModel

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        foodDetailViewModel =
            ViewModelProviders.of(this).get(FoodDetailViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_food_detail, container, false)
        initView(root)
        foodDetailViewModel.getmutableLiveFoodList().observe(viewLifecycleOwner, Observer {
            displayInfo(it)
        })
        return root
    }

    private fun displayInfo(it: Food?)
    {
        context?.let { it1 -> Glide.with(it1).load(Uri.parse(it!!.image)).into(img_food!!) }
        txtfoodName!!.text=StringBuilder(it!!.name)
        txtfoodDescription!!.text=StringBuilder(it!!.description)
        txtfoodPrice!!.text=StringBuilder(it!!.price.toString())

        for(size in it.size!!)
        {
            var radio_button = RadioButton(context)
            radio_button.setOnCheckedChangeListener { compoundButton, b ->
                    if (b)
                        common.food_selected!!.user_selected_size = size
                    calculateTotalPrice()

            }

            var params = LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.MATCH_PARENT, 1.0f
            )
            radio_button.layoutParams = params
            radio_button.text = size.name
            radio_button.tag = size.price
            radio_group_food_size!!.addView(radio_button)

        }
        if(radio_group_food_size!!.childCount>0)
        {
            val radioButton=radio_group_food_size!!.getChildAt(0)as RadioButton
            radioButton.isChecked=true
        }
    }

    private fun calculateTotalPrice()
    {
        var totalPrice=common.food_selected!!.price.toDouble()
        var displayPrice=0.0

        if((common.food_selected!!.user_selected_addOn != null && common.food_selected!!.user_selected_addOn!!.size > 0))
        {
            for(addon in common.food_selected!!.user_selected_addOn!!)
            {
                totalPrice +=addon.price.toDouble()
            }
        }

        totalPrice+= common.food_selected!!.user_selected_size!!.price.toDouble()
        displayPrice=totalPrice*number_button.number.toInt()
        displayPrice=(Math.round(displayPrice*100.0)/100).toDouble()
        showToast(requireContext(),displayPrice.toString())
        txtfoodPrice!!.text=StringBuilder("").append(common.formatPrice(displayPrice).toString())
    }

    private fun initView(root: View?)
    {

        compositeDisposable= CompositeDisposable()
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(context = requireContext()).cartDao())

        image_addOn=root!!.findViewById(R.id.img_addOn) as ImageView
        chip_Group_user_selected_addOn=root!!.findViewById(R.id.chip_group_user_selected_addOn) as ChipGroup

        addOn_bottom_Sheet_Dialog= (context?.let { BottomSheetDialog(it,R.style.DialogStyle) })!!
        var layout_user_selected_addOn=layoutInflater.inflate(R.layout.layout_add_on_display,null)
        chip_group_addOn=layout_user_selected_addOn.findViewById(R.id.chip_group_addOn) as ChipGroup
        editSearch=layout_user_selected_addOn.findViewById(R.id.editSearch) as EditText
        addOn_bottom_Sheet_Dialog.setContentView(layout_user_selected_addOn)


        addOn_bottom_Sheet_Dialog.setOnDismissListener{dialog->
            displayUserSelectedAddOn()
            calculateTotalPrice()
        }

        img_food = root!!.findViewById(R.id.image_food) as ImageView
        btnCart = root!!.findViewById(R.id.btnCart) as CounterFab
        txtfoodName = root!!.findViewById(R.id.txtFoodName) as TextView
        txtfoodDescription = root!!.findViewById(R.id.txtFoodDescription) as TextView
        txtfoodPrice = root!!.findViewById(R.id.txtFoodPrice) as TextView

        elegantNumber = root!!.findViewById(R.id.number_button) as ElegantNumberButton
        radio_group_food_size=root!!.findViewById(R.id.radio_group_size) as RadioGroup




        image_addOn!!.setOnClickListener{view->
            if(common.food_selected!!.addon != null)
            {
                displayAddOn()
                addOn_bottom_Sheet_Dialog.show()
            }
        }

        btnCart!!.setOnClickListener{
            var cart= CartItem()
            cart.userId=common.user_Id
            cart.userPhoneNumber=common.user_Id
            cart.foodId=common.food_selected!!.id
            cart.foodName=common.food_selected!!.name
            cart.foodImage=common.food_selected!!.image

            cart.foodPrice=common.food_selected!!.price.toDouble()
            cart.foodExtraPrice=common.calculateExtraPrice(common.food_selected!!.user_selected_size,common.food_selected!!.user_selected_addOn)
            cart.foodQuantity=number_button.number.toInt()
            if(common.food_selected!!.user_selected_addOn !=null)
                cart.addOn= Gson().toJson(common.food_selected!!.user_selected_addOn)
            else
                cart.addOn="DEFAULT"
            if(common.food_selected!!.user_selected_size !=null)
                cart.size=Gson().toJson(common.food_selected!!.user_selected_size)
            else
                cart.size="DEFAULT"


            cartDataSource!!.getItemWithOptionsInCart(common.user_Id!!,cart.foodId!!,cart.size!!,cart.addOn!!).subscribeOn(
                    Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object: SingleObserver<CartItem> {
                    override fun onSuccess(carItemFromDb: CartItem)
                    {

                        if(carItemFromDb.equals(cart))
                        {
                            carItemFromDb.foodExtraPrice=cart.foodExtraPrice
                            carItemFromDb.addOn=cart.addOn
                            carItemFromDb.size=cart.size
                            carItemFromDb.foodQuantity=carItemFromDb.foodQuantity+cart.foodQuantity

                            cartDataSource!!.update(carItemFromDb).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(object: SingleObserver<Int> {
                                    override fun onSuccess(t: Int)
                                    {
                                        showToast(context!!,"Update Cart Successful!")
                                        EventBus.getDefault().postSticky(CountCartEvent(true))
                                    }

                                    override fun onSubscribe(d: Disposable)
                                    {

                                    }

                                    override fun onError(e: Throwable)
                                    {
                                        showToast(context!!,"Update Cart "+e.message)

                                    }


                                })

                        }
                        else
                        {
                            compositeDisposable!!.add(cartDataSource!!.insertOrreplaceAll(cart).subscribeOn(
                                    Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    showToast(context!!,"Inserted into Cart!")
                                })
                            )
                        }
                    }

                    override fun onSubscribe(d: Disposable)
                    {

                    }

                    override fun onError(e: Throwable)
                    {
                        if(e.message!!.contains("empty"))
                        {
                            compositeDisposable!!.add(cartDataSource!!.insertOrreplaceAll(cart)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({
                                    showToast(context!!,"Inserted into Cart!")
                                    EventBus.getDefault().postSticky(CountCartEvent(true))
                                },{t: Throwable? ->
                                    context!!.deleteDatabase("EatOn");
                                    showToast(context!!,"Insert cart "+t!!.message)
                                }

                                )

                            )

                        }
                        else
                        {
                            showToast(context!!,"Cart Error!")
                        }
                    }

                })
        }
    }

    private fun displayAddOn()
    {
        if(common.food_selected!!.addon!!.size>0)
        {
            chip_group_addOn!!.clearCheck()
            chip_group_addOn!!.removeAllViews()
            editSearch!!.addTextChangedListener(this)
            for(addon in common.food_selected!!.addon!!)
            {
                var chip = layoutInflater.inflate(R.layout.layout_chip, null, false) as Chip
                chip.text = StringBuilder(addon.name).append("$+.").append(addon.price).append(")")
                    .toString()


                chip.setOnCheckedChangeListener { compoundButton, b ->
                    if (b) {
                        if (common.food_selected!!.user_selected_addOn == null)
                            common.food_selected!!.user_selected_addOn = ArrayList()
                        common.food_selected!!.user_selected_addOn!!.add(addon)
                    }
                }
                chip_group_addOn!!.addView(chip)

            }
        }
    }

    private fun displayUserSelectedAddOn()
    {
        if(common.food_selected!!.user_selected_addOn != null && common.food_selected!!.user_selected_addOn!!.size > 0)
        {
            chip_Group_user_selected_addOn!!.removeAllViews()

            for(addon in common.food_selected!!.user_selected_addOn!!)
            {
                var chip=layoutInflater.inflate(R.layout.layout_chip_with_delete,null,false) as Chip
                chip.text=StringBuilder(addon.name).append("+$").append(addon.price).append(")").toString()
                chip.isClickable=false
                chip.setOnCloseIconClickListener{view->
                    chip_Group_user_selected_addOn!!.removeView(view)
                    common.food_selected!!.user_selected_addOn!!.remove(addon)
                    calculateTotalPrice()
                }
                chip_Group_user_selected_addOn!!.addView(chip)
            }
        }
        else {
            if(common.food_selected!!.user_selected_addOn!!.size==0) {
                chip_Group_user_selected_addOn!!.removeAllViews()
            }
        }
    }

    override fun afterTextChanged(s: Editable?)
    {

    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int)
    {

    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
    {
        chip_group_addOn!!.clearCheck()
        chip_group_addOn!!.removeAllViews()

        for(addon in common.food_selected!!.addon!!)
        {
            if(addon.name.toLowerCase().contains(s.toString().toLowerCase())) {
                var chip = layoutInflater.inflate(R.layout.layout_chip, null, false) as Chip
                chip.text = StringBuilder(addon.name).append("$+.").append(addon.price).append(")")
                    .toString()


                chip.setOnCheckedChangeListener { compoundButton, b ->
                    if (b) {
                        if (common.food_selected!!.user_selected_addOn == null)
                            common.food_selected!!.user_selected_addOn = ArrayList()
                        common.food_selected!!.user_selected_addOn!!.add(addon)
                    }
                }
                chip_group_addOn!!.addView(chip)
            }
        }
    }


}