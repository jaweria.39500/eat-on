package pk.edu.iqra.eaton.CallBack

import android.view.View

interface IRecyclerItemClickListener {
    fun onItemClick(view: View, position: Int)
}