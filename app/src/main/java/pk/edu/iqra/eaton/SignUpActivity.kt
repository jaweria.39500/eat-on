package pk.edu.iqra.eaton

import ModelClasses.User
import Utils.common
import Utils.showToast
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    lateinit var context: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        context=this

        var database= Firebase.database
        var table_user=database.getReference("user")

        common.curEvent="SIGN_UP"
        btnSignUp2.setOnClickListener{

            val name=txtName.text.trim().toString()
            val phNum=txtPhNum.text.trim().toString()
            val password=txtPassword.text.trim().toString()
            val address=txt_address.text.trim().toString()

            if(common.curEvent=="SIGN_UP") {
                val postListener = object : ValueEventListener {

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        if (name == null || phNum == null || password == null || address == null) {
                            showToast(context, "Please enter all required fields!")

                        } else {
                            if (dataSnapshot.child(phNum).exists()) {
                                showToast(context, "Phone Number is already registered!")

                            } else {
                                val user = User(name, password, address)
                                table_user.child(phNum).setValue(user)
                                showToast(context, "Registered successfully!")
                                finish()

                                ///////////////Add Event to Analytics Log
                                common.logEvent("SignUp")
                            }
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                        showToast(context, "db error")
                    }

                }
                table_user.addValueEventListener(postListener)

            }
        }

    }
}

