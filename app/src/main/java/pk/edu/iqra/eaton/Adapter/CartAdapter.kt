package pk.edu.iqra.eaton.Adapter

import Utils.common
import Utils.showToast
import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton
import io.reactivex.disposables.CompositeDisposable
import org.greenrobot.eventbus.EventBus
import pk.edu.iqra.eaton.Database.CartItem
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.EventBus.UpdateItemCart
import pk.edu.iqra.eaton.ModelClasses.Category.Food
import pk.edu.iqra.eaton.R

class CartAdapter (var context: Context, var cartItem:List<CartItem> ): RecyclerView.Adapter<CartAdapter.CartViewHolder>(){


    private var compositeDisposable: CompositeDisposable?=null
    private var cartDataSource: ICartDataSource?=null

    init
    {
        compositeDisposable= CompositeDisposable()
        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(context).cartDao())
    }

    inner class CartViewHolder(view: View): RecyclerView.ViewHolder(view) {

        var txtFoodName: TextView? = null
        var txtFoodPrice: TextView? = null

        var image_cart: ImageView? = null
        var number_button:ElegantNumberButton?=null

        init {
            txtFoodName=view.findViewById(R.id.txt_cart_Food_Name) as TextView
            txtFoodPrice=view.findViewById(R.id.txt_cart_Food_Price) as TextView
            image_cart=view.findViewById(R.id.image_cart_food) as ImageView
            number_button=view.findViewById(R.id.cart_number_button) as ElegantNumberButton

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder
    {
        return CartViewHolder(LayoutInflater.from(context).inflate(R.layout.layout_display_cart_item,parent,false))

    }

    override fun getItemCount(): Int
    {
        return cartItem.size
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int)
    {
        Glide.with(context).load(Uri.parse(cartItem.get(position).foodImage!!))
            .into(holder.image_cart!!)
        holder.txtFoodName!!.text=StringBuilder(cartItem.get(position).foodName!!)
        var price:Double=(cartItem.get(position).foodPrice!!+cartItem.get(position).foodExtraPrice!!)
        holder.txtFoodPrice!!.text = price.toString();
        holder.number_button!!.number=cartItem.get(position).foodQuantity.toString()

        holder.number_button!!.setOnValueChangeListener{view,oldValue,newValue->
            cartItem[position].foodQuantity=newValue
            var cartItemChanged=cartItem[position]
            EventBus.getDefault().postSticky(UpdateItemCart(cartItemChanged))
            common.changedCart=cartItem[position]

        }





    }
}