package pk.edu.iqra.eaton.Database

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName="Cart",primaryKeys = ["FoodId","AddOn","Size","UserId"])
class CartItem {
    @NonNull
    @ColumnInfo(name="FoodId")
    var foodId:String?=null

    @ColumnInfo(name="FoodName")
    var foodName:String?=null

    @ColumnInfo(name="FoodPrice")
    var foodPrice:Double=0.0

    @ColumnInfo(name="FoodQuantity")
    var foodQuantity:Int=0

    @ColumnInfo(name="FoodImage")
    var foodImage:String?=null

    @NonNull
    @ColumnInfo(name="AddOn")
    var addOn:String?=null

    @NonNull
    @ColumnInfo(name="Size")
    var size:String?=null

    @ColumnInfo(name="UserPhoneNumber")
    var userPhoneNumber:String?=null

    @ColumnInfo(name="FoodExtraPrice")
    var foodExtraPrice:Double=0.0

    @NonNull
    @ColumnInfo(name="UserId")
    var userId:String?=null

    override fun equals(other: Any?): Boolean
    {
        if(other===this)
            return true
        if(other !is CartItem)
            return false
        var cartItem= other as CartItem?
        return cartItem!!.foodId==this.foodId && cartItem!!.size==this.size && cartItem!!.addOn==this.addOn
    }
}