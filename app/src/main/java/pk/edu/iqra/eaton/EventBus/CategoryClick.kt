package pk.edu.iqra.eaton.EventBus

import pk.edu.iqra.eaton.ModelClasses.Category.Menu

data class CategoryClick(var isSuccess:Boolean, var category: Menu)
