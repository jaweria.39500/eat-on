package pk.edu.iqra.eaton

import Utils.Constants
import Utils.common
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnLogIn.setOnClickListener{
            var intent= Intent(this,LogInActivity::class.java)
            startActivity(intent)
        }
        btnRegister.setOnClickListener{
            var intent= Intent(this,SignUpActivity::class.java)
            startActivity(intent)
        }
    }
    fun checkIfUserAlreadyRegistered() {
        val userId = common.sharedPreferences!!.getString(Constants.user_Id, "Invalid UserId")
        if (!userId!!.contentEquals("Invalid UserId")) {
            var intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()

        }
    }
}
