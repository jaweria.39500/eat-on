package pk.edu.iqra.eaton.ui.Home

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.Unbinder
import pk.edu.iqra.eaton.Adapter.PopularCategoryAdapter
import pk.edu.iqra.eaton.HomeActivity
import pk.edu.iqra.eaton.R

class HomeFragment : Fragment() {


    var recyclerViewCategory:RecyclerView?=null
    var unbinder: Unbinder?=null



    private lateinit var homeViewModel: HomeViewModel

    @SuppressLint("FragmentLiveDataObserve")
    override fun onCreateView(

        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {

        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        unbinder=ButterKnife.bind(this,root)
        initView(root)

        homeViewModel.categoryList.observe(
            viewLifecycleOwner,
            Observer {
                val listData = it
                val adapter= context?.let { it1 -> PopularCategoryAdapter(it1,listData) }

                recyclerViewCategory!!.adapter=adapter
            }
        )
        return root
    }

    private fun initView(root:View)
    {
        recyclerViewCategory=root.findViewById(R.id.recyclerViewCategory) as RecyclerView
        recyclerViewCategory?.setHasFixedSize(true)
        recyclerViewCategory?.layoutManager=LinearLayoutManager(context,RecyclerView.HORIZONTAL,false)

    }

}