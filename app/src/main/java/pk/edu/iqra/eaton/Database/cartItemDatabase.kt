package pk.edu.iqra.eaton.Database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(version=2,entities=[CartItem::class],exportSchema = false)
abstract class cartItemDatabase: RoomDatabase(){

    abstract fun cartDao(): ICartDao

    companion object{
        var instance :cartItemDatabase?=null

        fun getInstance(context: Context):cartItemDatabase
        {
            if(instance==null)
                instance= Room.databaseBuilder<cartItemDatabase>(context,cartItemDatabase::class.java!!,"EatOn").build()
            return instance!!
        }


    }
}