package pk.edu.iqra.eaton.Database

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface ICartDataSource {

    fun getAllCart(userId:String): Flowable<List<CartItem>>

    fun countItemCart(userId:String): Single<Int>

    fun sumPricce(userId:String): Single<Double>

    fun getItemInCart(userId:String,foodId:String): Single<CartItem>

    fun insertOrreplaceAll(vararg cart:CartItem): Completable

    fun update( cart:CartItem): Single<Int>

    fun deleteCart(cart: CartItem): Single<Int>

    fun cleanCart(userId:String): Single<Int>

    fun getItemWithOptionsInCart(userId:String,foodId:String,foodSize:String,foodAddOn:String):Single<CartItem>

}