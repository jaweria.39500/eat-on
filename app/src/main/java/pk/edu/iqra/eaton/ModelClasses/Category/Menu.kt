package pk.edu.iqra.eaton.ModelClasses.Category

import androidx.lifecycle.MutableLiveData

data class Menu (
    var foods: List<Food>?=null,
    var image:String="",
    var name:String=""
)