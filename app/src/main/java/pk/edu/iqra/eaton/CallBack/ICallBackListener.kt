package CallBack

import pk.edu.iqra.eaton.ModelClasses.Category.Menu


interface ICallBackListener {

    fun onLoadSuccess(category:List<Menu>)
    fun onLoadFailure(message:String)
}