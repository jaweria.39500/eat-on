package pk.edu.iqra.eaton.Database

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface ICartDao {
    @Query("Select * from Cart where UserId=:userId")
     fun getAllCart(userId:String):Flowable<List<CartItem>>

    @Query("Select sum(FoodQuantity) from Cart where UserId=:userId")
     fun countItemCart(userId:String):Single<Int>

    @Query("Select sum((FoodPrice + FoodExtraPrice)*FoodQuantity) from Cart where UserId=:userId")
     fun sumPricce(userId:String):Single<Double>

    @Query("Select * from Cart where foodId=:foodId And  UserId=:userId")
     fun getItemInCart(userId:String,foodId:String):Single<CartItem>

    @Insert(onConflict=OnConflictStrategy.REPLACE)
     fun insertOrreplaceAll(vararg cart:CartItem):Completable

    @Update(onConflict=OnConflictStrategy.REPLACE)
      fun update( cart:CartItem):Single<Int>

    @Delete
     fun deleteCart(cart: CartItem):Single<Int>

    @Query("Delete  from Cart where UserId=:userId")
     fun cleanCart(userId:String):Single<Int>

     @Query("Select * from Cart where FoodId=:foodId And UserId=:userId And Size=:foodSize And  AddOn=:foodAddOn")
    fun getItemWithOptionsInCart(userId:String,foodId:String,foodSize:String,foodAddOn:String):Single<CartItem>


}