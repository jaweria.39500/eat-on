package pk.edu.iqra.eaton

import Utils.Constants
import Utils.common
import Utils.showToast
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.navigation.NavController
import com.google.android.material.button.MaterialButton
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.app_bar_home.*
import kotlinx.android.synthetic.main.fragment_cart.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import pk.edu.iqra.eaton.Database.ICartDataSource
import pk.edu.iqra.eaton.Database.cartItemDatabase
import pk.edu.iqra.eaton.Database.localCartDataSource
import pk.edu.iqra.eaton.EventBus.*

class


HomeActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration

    var context:Context=this
    lateinit var  navController:NavController



    private var cartDataSource: ICartDataSource?=null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        cartDataSource= localCartDataSource(cartItemDatabase.getInstance(this).cartDao())


        val toolbar: Toolbar = toolbar
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.counter_fab_image_cart)
        fab.setOnClickListener { view ->
            navController.navigate(R.id.nav_cart)
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            topLevelDestinationIds = setOf(
                R.id.nav_home, R.id.nav_food_detail, R.id.nav_signOut,
                 R.id.nav_cart, R.id.nav_food_list
            ),
            drawerLayout = drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        countcartItem()


        val configSettings = FirebaseRemoteConfigSettings.Builder()
            .setDeveloperModeEnabled(BuildConfig.DEBUG)
            .build()
        common.remoteConfig?.setConfigSettings(configSettings)

        common.remoteConfig.setDefaults(R.xml.firebase_defaults)



    }

    override fun onResume()
    {
        super.onResume()
        countcartItem()
    }
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.home, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onStart()
    {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop()
    {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }
    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun onCategorySelected(event:CategoryClick)
    {
        if(event.isSuccess)
        {
            showToast(this,"Click to "+common.category_selected?.name)
            findNavController(R.id.nav_host_fragment).navigate(R.id.nav_food_list)

        }
    }
    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun onFoodSelected(event:FoodItemClick)
    {
        if(event.isSuccess)
        {
            showToast(this,"Click to "+common.food_selected?.name)
            findNavController(R.id.nav_host_fragment).navigate(R.id.nav_food_detail)

        }
    }
    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun cartEvent(event:CountCartEvent)
    {
        if(event.isSuccess)
        {
            countcartItem()
        }
    }

    fun countcartItem()
    {
        cartDataSource!!.countItemCart(common.user_Id!!).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<Int> {
                override fun onSuccess(t: Int)
                {
                    counter_fab_image_cart.count=t
                }

                override fun onSubscribe(d: Disposable)
                {

                }

                override fun onError(e: Throwable)
                {
                    showToast(context,"{Insert cart}"+e.message)
                    counter_fab_image_cart.count=0
                }

            })
    }
    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    fun hideFabCart(event:HideFabCart)
    {
        if(event.isHide)
        {
            counter_fab_image_cart.hide()
        }
        else
        {
            counter_fab_image_cart.show()
        }
    }
     fun getRemoteConfigValues(btnPlaceOrder:Button)
    {

        var cacheExpiration: Long = 7200

        common.remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful)
                {
                    var btn_place_order_visibility:Boolean=common.remoteConfig.getBoolean(Constants.Key_btn_Place_Order)
                    var serviceUnavailabilityReason:String=common.remoteConfig.getString(Constants.Key_Service_Unavailability_Reason)
                    var isServiceAvailable:Boolean=common.remoteConfig.getBoolean(Constants.Key_is_Service_Available)

                    common.serviceNotavailableBCO=serviceUnavailabilityReason
                    common.isServiceAvailable=isServiceAvailable

                }

            }
    }

}
